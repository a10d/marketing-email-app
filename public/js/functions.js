 jQuery(document).ready(function($){
  	/************
  	***  open profile nav
  	********************/
  	$(document).on('click' ,".dropdown-user" ,function(event){
  		$('.profile-nav').toggleClass('open');
  	});

  	/***********
  	***** Close profile when click outside
  	***************/
   $(document).on("click", function(event){
      var $trigger = $(".profile-btn");
          if($trigger !== event.target && !$trigger.has(event.target).length){
            $('.profile-nav').removeClass('open');
        }       
    });
   /***********
   ******** toggle sidebar
   *****************/
   var viewportWidth = $(window).width();
    if (viewportWidth <  767) {
   		$('body').removeClass('sidebar-collapse');
    }else{
   		$('body').removeClass('sidebar-open');        	
    }
   $(document).on("click",'.nav-collapse-btn', function(event){
   	 	if (viewportWidth >=  767) {
	   		$('body').removeClass('sidebar-open');        	
	   		$('body').toggleClass('sidebar-collapse');
	    }else{
	   		$('body').removeClass('sidebar-collapse');
	   		$('body').toggleClass('sidebar-open');
	    }
   });
   /*******
   **** Contenet min height
   *********/
   if ($('.page-content').length != 0) {
   		var min_height = $(this).height();
   		console.log(min_height);
   		$('.sh_content').css({"min-height": min_height});
   }
   /**********
   ***** Next compaign
   ***********/
   $(document).on('click' ,".next-btn" ,function(event){
   		var step_id	 =$(this).closest('.tab-conent').next().attr('id');
	   	$(this).closest('.tab-conent').removeClass('current').next('.tab-conent').addClass('current');
	   	console.log("a[href= '#"+step_id+"']");
	   	$("a[href= '#"+step_id+"']").closest('li').addClass('active');
	   	if (step_id == "step-2") {
	   		$('.compaing-head .h4').text('Choose Design');
	   		$('.compaing-head .declare-title').text('Entered campaign name will be here');
	   		$('.compaing-head .select-type-compaign li:first-of-type ').attr("class",'select-one active');
	   		$('.compaing-head .select-type-compaign li:first-of-type a').attr("data-tab",'designs');
	   		$('.compaing-head .select-type-compaign li:first-of-type i').attr("class",'mdi-folder-multiple-image mdi');
	   		$('.compaing-head .select-type-compaign li:first-of-type span').text('Select Template');
	   		$('.compaing-head .select-type-compaign li:nth-child(2) i').attr("class",' mdi-editor-format-color-fill mdi');
	   		$('.compaing-head .select-type-compaign li:nth-child(2) a').attr("data-tab",'layouts');
	   		$('.compaing-head .select-type-compaign li:nth-child(2) ').attr("class",'build-one');
	   		$('.select-type-compaign li:nth-child(2) span').text("Build your own");
	   	}
   });
   /********** 
   **********Design tabs
   *********/
   $(document).on('click' ,".select-type-compaign li a" ,function(event){
   	var tab_ref = $(this).attr('data-tab');
   	$('.select-type-compaign li').removeClass('active');
   	$('.tabs-design').removeClass('current');
   	$(this).closest('li').addClass('active');
   	$('.'+tab_ref).addClass('current');

   });
   /*********
   ****** Knob 
   **********/
     $(function () {
    /* jQueryKnob */

    $(".knob").knob({
      draw: function () {

        // "tron" case
        if (this.$.data('skin') == 'tron') {

          var a = this.angle(this.cv)  // Angle
              , sa = this.startAngle          // Previous start angle
              , sat = this.startAngle         // Start angle
              , ea                            // Previous end angle
              , eat = sat + a                 // End angle
              , r = true;

          this.g.lineWidth = this.lineWidth;

          this.o.cursor
          && (sat = eat - 0.3)
          && (eat = eat + 0.3);

          if (this.o.displayPrevious) {
            ea = this.startAngle + this.angle(this.value);
            this.o.cursor
            && (sa = ea - 0.3)
            && (ea = ea + 0.3);
            this.g.beginPath();
            this.g.strokeStyle = this.previousColor;
            this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
            this.g.stroke();
          }

          this.g.beginPath();
          this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
          this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
          this.g.stroke();

          this.g.lineWidth = 2;
          this.g.beginPath();
          this.g.strokeStyle = this.o.fgColor;
          this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
          this.g.stroke();

          return false;
        }
      }
    });
    /* END JQUERY KNOB */

    //INITIALIZE SPARKLINE CHARTS
    $(".sparkline").each(function () {
      var $this = $(this);
      $this.sparkline('html', $this.data());
    });

    /* SPARKLINE DOCUMENTATION EXAMPLES http://omnipotent.net/jquery.sparkline/#s-about */
    // drawDocSparklines();
    // drawMouseSpeedDemo();

  });
var ctx = $(".chart.has-fixed-height ");
ctx.height = 200;






});