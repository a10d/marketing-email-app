@extends('layouts.frontend')

@section('title', trans('messages.Create_automation'))
	
@section('page_script')
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
		
    <script type="text/javascript" src="{{ URL::asset('js/validate.js') }}"></script>
@endsection

@section('page_header')	
			<div class="page-title">
				<h1>
					<span class="text-semibold">Create New Automation</span>
				</h1>				
			</div>
		
@endsection

@section('content')
        	<div class="panel-card width-80">
                <form action="{{ action("AutomationController@store") }}" method="POST" class="form-validate-jqueryz">
                    {{ csrf_field() }}
                    
					@include("automations._form")
					
					<hr>
					
					<div class="text-right">
						<button class="btn bg-teal-800">{{ trans('messages.Create') }} <i class="icon-arrow-right7"></i> </button>
					</div>
					
				<form>
			</div>
@endsection
