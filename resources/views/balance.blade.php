@extends('layouts.frontend')

@section('title', trans('messages.dashboard'))

@section('page_script')
    <script type="text/javascript" src="{{ URL::asset('assets/js/plugins/visualization/echarts/echarts.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/chart.js') }}"></script>
@endsection

@section('content')


    <p class=" h4 inline widget-title">Balance ,</p><p class="gold inline"> free plan is active</p>
    <div class="row">
        <div class="col-xs-12 col-md-4">
            <div class="panel-card">
                <div class="card-title gold">{{ trans('messages.campaigns') }}</div>
                <div class="circle-percentage">
                    <div class="circle pull-left">
                        <input type="text" class="knob golden" value="{{ Auth::user()->customer->campaignsUsage() }}" data-width="90" data-height="90" data-fgColor="#CBA661" disabled data-readonly="true">
                    </div>
                    <div class="data-num pull-right">
                        Usages
                        <p>{{ \Acelle\Library\Tool::format_number(Auth::user()->customer->campaignsCount()) }} <strong>/ {{ \Acelle\Library\Tool::format_number(Auth::user()->customer->maxCampaigns()) }}</strong></p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-xs-12 col-md-4">
            <div class="panel-card">
                <div class="card-title purple">{{ trans('messages.subscribers') }}</div>
                <div class="circle-percentage">
                    <div class="circle pull-left">
                        <input type="text" class="knob golden" value="{{ Auth::user()->customer->readCache('SubscriberUsage', 0) }}" data-width="90" data-height="90" data-fgColor="#8d1040" disabled data-readonly="true">
                    </div>
                    <div class="data-num pull-right">
                        Usages
                        <p>{{ \Acelle\Library\Tool::format_number(Auth::user()->customer->readCache('SubscriberCount', 0)) }} <strong>/ {{ \Acelle\Library\Tool::format_number(Auth::user()->customer->maxSubscribers()) }}</strong></p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-xs-12 col-md-4">
            <div class="panel-card">
                <div class="card-title green">{{ trans('messages.lists') }}</div>
                <div class="circle-percentage">
                    <div class="circle pull-left">
                        <input type="text" class="knob golden" value="{{ Auth::user()->customer->listsUsage() }}" data-width="90" data-height="90" data-fgColor="#46b94f" disabled data-readonly="true">
                    </div>
                    <div class="data-num pull-right">
                        Usages
                        <p>{{ \Acelle\Library\Tool::format_number(Auth::user()->customer->listsCount()) }} <strong>/ {{ \Acelle\Library\Tool::format_number(Auth::user()->customer->maxLists()) }}</strong></p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="clearifx"></div>
    </div><!-- /row -->

    <div class="row mid-margin-top">
        <div class="col-xs-12 col-md-3">
            <h4 class="  widget-title">Quick Action</h4>
            <div class="panel-card">
                <a href="{{ action('MailListController@index') }}" class="gold-btn fix-width m-center"                  
                    rel0="MailListController"
                    rel1="FieldController"
                    rel2="SubscriberController"
                    rel3="SegmentController"
                >
                    Import {{ trans('messages.lists') }}
                </a>
                <a href="javascript:void(0)" class="theme-btn fix-width">
                    Create Template
                </a>
                <a href="javascript:void(0)" class="theme-btn fix-width">
                    Create Autoresponder
                </a>
                <a href="javascript:void(0)" class="theme-btn fix-width">
                    Create Form
                </a>
            </div>
        </div><!-- /cols -->
        <div class="col-xs-12 col-md-9">
            <h4 class="widget-title ">24-hour performance <p class="inline tiny-font"> Below are the details of subscriber activity (opens and clicks) measured for this campaign during the last 24 hours.</p></h4>
        {{-- <h3 class="text-teal-800 mt-40"><i class="icon-paperplane"></i> {{ trans('messages.recently_sent_campaigns') }}</h3> --}}
            @if (Auth::user()->customer->sentCampaigns()->count() == 0)
                <div class="empty-list">
                    <i class="icon-paperplane"></i>
                    <span class="line-1">
                        {{ trans('messages.no_sent_campaigns') }}
                    </span>
                </div>
            @else
                <div class="row">
                    <div class="col-md-6">
                        @include('helpers.form_control', [
                            'type' => 'select',
                            'class' => 'dashboard-campaign-select',
                            'name' => 'campaign_id',
                            'label' => '',
                            'value' => '',
                            'options' => Acelle\Model\Campaign::getSelectOptions(Auth::user()->customer, "done"),
                        ])
                    </div>
                </div>
                <div class="campaign-quickview-container" data-url="{{ action("CampaignController@quickView") }}"></div>
            @endif
        </div><!-- /cols -->
        <div class="clearfix"></div>
    </div><!-- /row -->




    <h4 class="widget-title "> Sent Campaign Reports</h4>
    <div class="tabbable">
{{--         <ul class="nav nav-tabs nav-tabs-top">
            <li class="active text-semibold"><a href="#top-tab1" data-toggle="tab">
                <i class="icon-folder-open3"></i> {{ trans('messages.campaign_opens') }}</a></li>
            <li class="text-semibold"><a href="#top-tab2" data-toggle="tab">
                <i class="icon-pointer"></i> {{ trans('messages.campaign_clicks') }}</a></li>
            <li class="text-semibold"><a href="#top-tab3" data-toggle="tab">
                <i class="icon-link"></i> {{ trans('messages.clicked_links') }}</a></li>
        </ul>
 --}}        <div class="tab-content">
            <div class="tab-pane active" id="top-tab1">
                <ul class="modern-listing mt-0 top-border-none">
                    <li>
                        <div class="row">
                            <div class="col-sm-4 col-md-4">
                                Campaign
                            </div>
                            <div class="col-sm-2 col-md-2 text-left">
                                Sent
                            </div>
                            <div class="col-sm-1 col-md-1 text-left">
                                Reciptients
                            </div>
                            <div class="col-sm-1 col-md-1 text-left">
                                Opened
                            </div>
                            <div class="col-sm-1 col-md-1 text-left">
                                Clicked
                            </div>
                            <div class="col-sm-3 col-md-3 text-left">
                                Actions
                            </div>
                        </div>
                    </li>
                    @forelse (Acelle\Model\Campaign::topOpens(10, Auth::user()->customer)->get() as $num => $item)
                        <li>
                            <div class="row">
                                <div class="col-sm-4 col-md-4">
                                    <h6 class="mt-0 mb-0 text-semibold">
                                        <a href="{{ action('CampaignController@overview', $item->uid) }}">
                                            {{ $item->name }}
                                        </a>
                                    </h6>
{{--                                     <p>
                                        {!! $item->displayRecipients() !!}
                                    </p>
 --}}                           </div>
                                <div class="col-sm-2 col-md-2 text-left">
                                    <h5 class="no-margin text-bold">
                                       {{--  {{ (null !== $item->lastOpen()) ? Acelle\Library\Tool::formatDateTime($item->lastOpen()->created_at) : "" }} --}}
                                    @if ($item->status == 'new')
                                        <span class="text-muted2">{{ trans('messages.run_at') }}: &nbsp;&nbsp;<i class="icon-alarm mr-0"></i> {{ isset($item->run_at) ? Tool::formatDateTime($item->run_at) : "" }}</span>
                                    @else
                                        <span class="text-muted2">{{ Tool::formatDateTime($item->created_at) }}</span>
                                    @endif
                                    </h5>
                                </div>
                                <div class="col-sm-1 col-md-1 text-left">
                                    <h5 class="no-margin text-bold">
                                        {{ number_with_delimiter($item->readCache('SubscriberCount')) }}
                                    </h5>
                                </div>
                                <div class="col-sm-1 col-md-1 text-left">
                                    <h5 class="no-margin text-bold">
                                    {{--{{ $item->aggregate }}--}}
                                    {{ $item->readCache('UniqOpenRate') }}%
                                    </h5>
                                        <br /><br />
                                </div>
                                <div class="col-sm-1 col-md-1 text-left">
                                    <h5 class="no-margin text-bold">
                                        {{-- {{ number_with_delimiter($item->readCache('UniqOpenCount')) }} --}}
                                        {{ $item->readCache('ClickedRate') }}%
                                    </h5>
{{--                                     <span class="text-muted">{{ trans('messages.uniq_opens') }}</span>
                                        <br /><br />
 --}}                           </div>
                                <div class="col-sm-3 col-md-3 text-left">
                                    @if (\Gate::allows('update', $item))
                                        <a href="{{ action('CampaignController@edit', $item->uid) }}" type="button" class="btn bg-grey btn-icon"> <i class="icon-pencil"></i> {{ trans('messages.edit') }}</a>
                                    @endif
                                    @if (\Gate::allows('overview', $item))
                                        <a href="{{ action('CampaignController@overview', $item->uid) }}" data-popup="tooltip" title="{{ trans('messages.overview') }}" type="button" class="btn bg-teal-600 btn-icon"><i class="icon-stats-growth"></i> {{ trans('messages.overview') }}</a>
                                    @endif
                                    @if (\Gate::allows('delete', $item) || \Gate::allows('pause', $item) || \Gate::allows('restart', $item))
                                        <div class="btn-group">
                                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><span class="caret ml-0"></span></button>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                @if (\Gate::allows('resend', $item))
                                                    <li><a data-method='POST' link-confirm="{{ trans('messages.campaign.resend_confirm') }}" href="{{ action('CampaignController@resend', ["uids" => $item->uid]) }}">
                                                        <i class="icon-reload-alt"></i> {{ trans("messages.campaign.resend") }}</a></li>
                                                @endif
                                                @if (\Gate::allows('pause', $item))
                                                    <li><a link-confirm="{{ trans('messages.pause_campaigns_confirm') }}" href="{{ action('CampaignController@pause', ["uids" => $item->uid]) }}"><i class="icon-pause"></i> {{ trans("messages.pause") }}</a></li>
                                                @endif
                                                @if (\Gate::allows('restart', $item))
                                                    <li><a link-confirm="{{ trans('messages.restart_campaigns_confirm') }}" href="{{ action('CampaignController@restart', ["uids" => $item->uid]) }}"><i class="icon-history"></i> {{ trans("messages.restart") }}</a></li>
                                                @endif
                                                @if (\Gate::allows('copy', $item))
                                                    <li>
                                                        <a data-uid="{{ $item->uid }}" data-name="{{ trans("messages.copy_of_campaign", ['name' => $item->name]) }}" class="copy-campaign-link">
                                                            <i class="icon-copy4"></i> {{ trans('messages.copy') }}
                                                        </a>
                                                    </li>
                                                @endif
                                                @if (\Gate::allows('delete', $item))
                                                    <li><a delete-confirm="{{ trans('messages.delete_campaigns_confirm') }}" href="{{ action('CampaignController@delete', ["uids" => $item->uid]) }}"><i class="icon-trash"></i> {{ trans("messages.delete") }}</a></li>
                                                @endif
                                            </ul>
                                        </div>
                                    @endif
                               </div>
                            </div>

                        </li>
                    @empty
                        <li class="empty-li">
                            {{ trans('messages.empty_record_message') }}
                        </li>
                    @endforelse
                </ul>
            </div>
{{--             <div class="tab-pane" id="top-tab2">
                <ul class="modern-listing mt-0 top-border-none">
                    @forelse (Acelle\Model\Campaign::topClicks(5, Auth::user()->customer)->get() as $num => $item)
                        <li>
                            <div class="row">
                                <div class="col-sm-5 col-md-5">
                                    <i class="number">{{ $num+1 }}</i>
                                    <h6 class="mt-0 mb-0 text-semibold">
                                        <a href="{{ action('CampaignController@overview', $item->uid) }}">
                                            {{ $item->name }}
                                        </a>
                                    </h6>
                                    <p>
                                        {!! $item->displayRecipients() !!}
                                    </p>
                                </div>
                                <div class="col-sm-2 col-md-2 text-left">
                                    <h5 class="no-margin text-bold">
                                        {{ $item->aggregate }}
                                    </h5>
                                    <span class="text-muted">{{ trans('messages.clicks') }}</span>
                                        <br /><br />
                                </div>
                                <div class="col-sm-2 col-md-2 text-left">
                                    <h5 class="no-margin text-bold">
                                        {{ $item->urlCount() }}
                                    </h5>
                                    <span class="text-muted">{{ trans('messages.urls') }}</span>
                                        <br /><br />
                                </div>
                                <div class="col-sm-2 col-md-2 text-left">
                                    <h5 class="no-margin text-bold">
                                        {{ (null !== $item->lastClick()) ? Acelle\Library\Tool::formatDateTime($item->lastClick()->created_at) : "" }}
                                    </h5>
                                    <span class="text-muted">{{ trans('messages.last_clicked') }}</span>
                                        <br /><br />
                                </div>
                            </div>
                        </li>
                    @empty
                        <li class="empty-li">
                            {{ trans('messages.empty_record_message') }}
                        </li>
                    @endforelse
                </ul>
            </div>
            <div class="tab-pane" id="top-tab3">
                <ul class="modern-listing mt-0 top-border-none">
                    @forelse (Acelle\Model\Campaign::topLinks(5, Auth::user()->customer)->get() as $num => $item)
                        <li>
                            <div class="row">
                                <div class="col-sm-6 col-md-6">
                                    <i class="number">{{ $num+1 }}</i>
                                    <h6 class="mt-0 mb-0 text-semibold url-truncate">
                                        <a title="{{ $item->url }}" href="{{ $item->url }}" target="_blank">
                                            {{ $item->url }}
                                        </a>
                                    </h6>
                                    <p>
                                        {{ $item->campaigns()->count() }} {{ trans('messages.campaigns') }}
                                    </p>
                                </div>
                                <div class="col-sm-2 col-md-2 text-left">
                                    <h5 class="no-margin text-bold">
                                        {{ $item->aggregate }}
                                    </h5>
                                    <span class="text-muted">{{ trans('messages.clicks') }}</span>
                                        <br /><br />
                                </div>
                                <div class="col-sm-2 col-md-2 text-left">
                                    <h5 class="no-margin text-bold">
                                        {{ (null !== $item->lastClick(Auth::user()->customer)) ? Acelle\Library\Tool::formatDateTime($item->lastClick(Auth::user()->customer)->created_at) : "" }}
                                    </h5>
                                    <span class="text-muted">{{ trans('messages.last_clicked') }}</span>
                                    <br /><br />
                                </div>
                            </div>
                        </li>
                    @empty
                        <li class="empty-li">
                            {{ trans('messages.empty_record_message') }}
                        </li>
                    @endforelse
                </ul>
            </div> --}}
        </div>
    </div>

    <br>
    <br>
@endsection
