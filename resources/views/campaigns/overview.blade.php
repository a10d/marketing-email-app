@extends('layouts.frontend')

@section('title', $campaign->name)

@section('page_script')
    <script type="text/javascript" src="{{ URL::asset('assets/js/plugins/visualization/echarts/echarts.js') }}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/chart.js') }}"></script>
@endsection

@section('page_header')
    <div class="panel-card">
			@include("campaigns._header")
	</div>

@endsection

@section('content')
    <div class="panel-card">
            @include("campaigns._menu")

			@include("campaigns._info")
	</div>

            <br />
    <div class="panel-card">
            @include("campaigns._chart")
	</div>

            <hr />
    <div class="panel-card">
			@include("campaigns._open_click_rate")
	</div>
	    <div class="panel-card">
			@include("campaigns._count_boxes")
	</div>
            <br />
	    <div class="panel-card">
            @include("campaigns._24h_chart")
	</div>

            <br />
	    <!--<div class="panel-card">-->
			@include("campaigns._top_link")
	<!--</div>-->
            <br />
	    <div class="panel-card">
			@include("campaigns._most_click_country")
	</div>
            <br />
	    <div class="panel-card">
			@include("campaigns._most_open_country")
	</div>
			<br />
	    <div class="panel-card">
			@include("campaigns._most_open_location")
	</div>
            <br />
@endsection
