@extends('layouts.frontend')

@section('title', trans('messages.campaigns') . " - " . trans('messages.setup'))
	
@section('page_script')
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
		
    <script type="text/javascript" src="{{ URL::asset('js/validate.js') }}"></script>
@endsection

@section('page_header')
	
			<div class="page-title">
				<h1>
					<span class="text-semibold"> {{ $campaign->name }}</span>
				</h1>

				@include('campaigns._steps', ['current' => 2])
			</div>

@endsection

@section('content')
                <form action="{{ action('CampaignController@setup', $campaign->uid) }}" method="POST" class="form-validate-jqueryz">
					{{ csrf_field() }}
					
					<div class="row">
						<div class="col-md-6 col-md-offset-3 list_select_box" target-box="segments-select-box" segments-url="{{ action('SegmentController@selectBox') }}">

                          <div class="form-group">
                            <label for="campaignName">Name This Campaign </label>
                            <small class="form-text text-muted">The Campaign name is shown in your reports and your email archive.</small>
                            <p class="shading-input">
                            <input type="text" name ="name" class="form-control" id="campaignName" value="{{ $campaign->name }}"></p>
                          </div>

                          <div class="form-group">
                            <label for="campaignSubject">Write a subject line </label>
                            <small class="form-text text-muted">The subject of your Campaign</small>
                            <p class="shading-input">
                            <input type="text" name ="subject" class="form-control" id="campaignSubject" ></p>
                          </div>

                          <div class="form-group">
                            <label for="campaignFrom">Who is it from? </label>
                            <small class="form-text text-muted">This will display in the from field</small>

                            <!--<div class="row">-->
                            <p class="shading-input">
                                <input type="text" name ="from_name" class="form-control input-50" id="campaignFrom" value="{{$campaign->from_name}}">
                                
                                <input type="email" name ="from_email" class="form-control input-50" id="campaignFromMail" value="{{$campaign->from_email}}">
                            </p>
                            <!--</div>-->

                          </div>

                          <div class="form-group">
                            <label for="campaignReply">Reply to? </label>
                            <small class="form-text text-muted">This will display in the reply-to field</small>
                            <p class="shading-input">
                            <input type="email" name ="reply_to" class="form-control" id="campaignReply" value="{{$campaign->from_email}}"></p>
                          </div>


                                                            
						</div>
{{-- 						<div class="col-md-6 segments-select-box">
                            <div class="form-group checkbox-right-switch">
                                @include('helpers.form_control', ['type' => 'checkbox',
                                                                'name' => 'track_open',
                                                                'label' => trans('messages.track_opens'),
                                                                'value' => $campaign->track_open,
                                                                'options' => [false,true],
                                                                'help_class' => 'campaign',
                                                                'rules' => $rules
                                                            ])
                                
                                @include('helpers.form_control', ['type' => 'checkbox',
                                                                'name' => 'track_click',
                                                                'label' => trans('messages.track_clicks'),
                                                                'value' => $campaign->track_click,
                                                                'options' => [false,true],
                                                                'help_class' => 'campaign',
                                                                'rules' => $rules
                                                            ])
                                
                                @include('helpers.form_control', ['type' => 'checkbox',
                                                                'name' => 'sign_dkim',
                                                                'label' => trans('messages.sign_dkim'),
                                                                'value' => $campaign->sign_dkim,
                                                                'options' => [false,true],
                                                                'help_class' => 'campaign',
                                                                'rules' => $rules
                                                            ])
                            </div>
						</div> --}}
					</div>
					<hr>
					<!--<div class="text-right">-->
						<button class="btn bg-teal-800 m-center next-btn">{{ trans('messages.next') }} <i class="icon-arrow-right7 "></i> </button>
					<!--</div>-->
					
				<form>
					
				
@endsection
