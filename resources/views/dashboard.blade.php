@extends('layouts.frontend')

@section('title', trans('messages.dashboard'))

@section('page_script')
    <script type="text/javascript" src="{{ URL::asset('assets/js/plugins/visualization/echarts/echarts.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/chart.js') }}"></script>
@endsection

@section('content')
    <div class="dashboard-page">
        <!-- <div class="container"> -->
            <div class="h2 text-center">Hi {{ Auth::user()->customer->displayName() }}! Welcome back to the Dashboard</div>
            <p class="text-center">Seems like you haven’t started yet, follow the easiest steps given below and get started your journey.</p>
            <div class="graph-contain lg-margin">
                <div class="graph-item">
                    <img src="images/app.pdf.png" alt=" " class="img-responsive m-center">
                    <p class="sm-margin-top">Choose and customize the template</p>
                </div>
                <div class="arrow">
                    <i class="mdi mdi-share"></i>
                </div>
                <div class="graph-item">
                    <img src="images/aaf.png" alt=" " class="img-responsive m-center">
                    <p class="sm-margin-top">Add your lists/import lists</p>
                </div>
                <div class="arrow">
                    <i class="mdi mdi-share mdi-flip-v"></i>
                </div>
                <div class="graph-item">
                    <img src="images/d.pdf.png" alt=" " class="img-responsive m-center">
                    <p class="sm-margin-top">Send and track your compaign</p>
                </div>
            </div>
            <a href="{{ action('CampaignController@index') }}" class="gold-btn btn-padding-1 m-center" rel0="CampaignController">
                CREATE {{ trans('messages.campaigns') }}
            </a>
        <!-- </div>/container -->
    </div><!-- /dashboard-page -->
@endsection


