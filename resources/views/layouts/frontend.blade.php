<!DOCTYPE html>
<html lang="en">
<head>
	<title>@yield('title') - {{ \Acelle\Model\Setting::get("site_name") }}</title>

	@include('layouts._favicon')

	@include('layouts._head')

	@include('layouts._css')

	@include('layouts._js')

	<script>
		$.cookie('last_language_code', '{{ Auth::user()->customer->getLanguageCode() }}');
	</script>

</head>

<body class="color-scheme-{{ Auth::user()->customer->getColorScheme() }}">
	<div class="sh_sidebar">
		<div class="sh_sidebar_inner">
			<div class="logo black_background padding">
				<a href="{{ action('HomeController@index') }}">
					<img src="{{ URL::asset('/images/logo_big.png') }}" alt="" class="img-responsive full-width">
				</a>
			</div>
			<ul class="dashboard-nav">
				<li rel0="HomeController">
					<a href="{{ action('HomeController@index') }}">
						<i class="mdi mdi-apps"></i>
						{{ trans('messages.dashboard') }}
					</a>
				</li>
				
			
				
				<li rel0="CampaignController">
					<a href="{{ action('CampaignController@index') }}">
						<i class="mdi mdi-email"></i>
						{{ trans('messages.campaigns') }}
					</a>
				</li>
				<li rel0="AutomationController">
					<a href="{{ action('AutomationController@index') }}">
						<i class=" mdi mdi-alarm-check"></i>
						{{ trans('messages.Automations') }}
					</a>
				</li>
				<li
					rel0="MailListController"
					rel1="FieldController"
					rel2="SubscriberController"
					rel3="SegmentController"
				>
					<a href="{{ action('MailListController@index') }}">
						<i class="mdi mdi-playlist-plus"></i>
						{{ trans('messages.lists') }}
					</a>
				</li>
				
				                <li rel0="TemplateController">
					<a href="{{ action('TemplateController@index') }}">
						<i class="icon-magazine"></i> {{ trans('messages.templates') }}
					</a>
				</li>
				
				
				
				@if (Auth::user()->customer->can("read", new Acelle\Model\Subscription()))
				<li rel0="AccountController\subscription">
					<a href="{{ action('AccountController@subscription') }}">
						<i class="mdi mdi-receipt"></i>
						{{ trans('messages.subscriptions') }}
					</a>
				</li>
				@endif
				<li>
					<a href="javascript:void(0)">
						<i class="icon-graph"></i>
						Reports
					</a>
				</li>
			</ul>
			<a href="javascript:void(0)" class="support">
				<i class="icon-question"></i>
				Support
			</a>
		</div>
	</div><!-- sidebar -->	
	<div class="sh_content">
		<!-- Main navbar -->
		<div class="navbar navbar-{{ Auth::user()->customer->getColorScheme() == "white" ? "default" : "inverse" }}">
			<div class="navbar-header">
				<ul class="nav navbar-nav pull-right visible-xs-block">
					<li><a class="mobile-menu-button" data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-menu7"></i></a></li>
				</ul>
			</div>

			<div class="navbar-collapse collapse" id="navbar-mobile">

				<div class="nav-bar_left pull-left">
					<button class="nav-collapse-btn display-inline">
						<i class="mdi mdi-view-headline"></i>
					</button>
					<div class="display-inline hidden-xs hidden-sm">
						<div class="h4 m-0">@yield('title')</div>							
						<p>Welcome {{Auth::user()->customer->displayName()}} </p>								
					</div>
				</div>

			<ul class="nav navbar-nav navbar-right">
				<!--<li class="dropdown language-switch">
					<a class="dropdown-toggle" data-toggle="dropdown">
						{{ Acelle\Model\Language::getByCode(Config::get('app.locale'))->name }}
						<span class="caret"></span>
					</a>

					<ul class="dropdown-menu">
						@foreach(Acelle\Model\Language::getAll() as $language)
							<li class="{{ Acelle\Model\Language::getByCode(Config::get('app.locale'))->code == $language->code ? "active" : "" }}">
								<a>{{ $language->name }}</a>
							</li>
						@endforeach
					</ul>
                </li>-->

				<!--<li class="dropdown">
					<a href="#" class="dropdown-toggle top-quota-button" data-toggle="dropdown" data-url="{{ action("AccountController@quotaLog") }}">
						<i class="icon-stats-bars4"></i>
						<span class="visible-xs-inline-block position-right">{{ trans('messages.used_quota') }}</span>
					</a>
				</li>-->

                	<ul class="nav navbar-nav navbar-right">
					<li>
						<a href="javascript:void(0)" class="gold-btn btn-padding-0">
							upgrade
						</a>
					</li>

				@include('layouts._top_activity_log')

				<li class="dropdown dropdown-user">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="{{ action('CustomerController@avatar', Auth::user()->customer->uid) }}" alt="">
						<span>{{ Auth::user()->customer->displayName() }}</span>
						<i class="caret"></i>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
					    
					    
					    
					    
						@can("admin_access", Auth::user())
							<li><a href="{{ action("Admin\HomeController@index") }}"><i class="icon-enter2"></i> {{ trans('messages.admin_view') }}</a></li>
							<li class="divider"></li>
						@endif
						<li class="dropdown">
							<a href="#" class="top-quota-button" data-url="{{ action("AccountController@quotaLog") }}">
								<i class="icon-stats-bars4"></i>
								<span class="">{{ trans('messages.used_quota') }}</span>
							</a>
						</li>
						<li><a href="{{ action("HomeController@balance") }}"><i class="icon-credit-card"></i> Balance</a></li>
						
						@if (Auth::user()->customer->can("read", new Acelle\Model\Subscription()))
							<li rel0="AccountController\subscription">
								<a href="{{ action('AccountController@subscription') }}">
									<i class="icon-quill4"></i> {{ trans('messages.subscriptions') }}
								</a>
							</li>
						@endif
						
					
						
				
						
						<li><a href="{{ action("AccountController@profile") }}"><i class="icon-profile"></i> {{ trans('messages.account') }}</a></li>
						@if (Auth::user()->customer->canUseApi())
							<li rel0="AccountController/api">
								<a href="{{ action("AccountController@api") }}" class="level-1">
									<i class="icon-key position-left"></i> {{ trans('messages.api') }}
								</a>
							</li>
						@endif
						<li><a href="{{ url("/logout") }}"><i class="icon-switch2"></i> {{ trans('messages.logout') }}</a></li>
					</ul>
				</li>
			</ul>
			</div>
		</div>
		<!-- /main navbar -->

		<!-- Page header -->
		<div class="page-header">
			<div class="page-header-content">

				@yield('page_header')

			</div>
		</div>
		<!-- /page header -->

		<!-- Page container -->
		<div class="page-container">

			<!-- Page content -->
			<div class="page-content">

				<!-- Main content -->
				<div class="content-wrapper">

					<!-- display flash message -->
					@include('common.errors')

					<!-- main inner content -->
					@yield('content')

				</div>
				<!-- /main content -->

			</div>
			<!-- /page content -->


			<!-- Footer -->
			<!--<div class="footer text-muted">
				{!! trans('messages.copy_right') !!}
			</div>-->
			<!-- /footer -->

		</div>
		<!-- /page container -->

		@include("layouts._modals")
	</div>
</body>
</html>
